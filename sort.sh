#!/bin/bash

dateStart=$(date +%H%M%S)

dirEntry=~/your/start/dir
dirDest=~/your/destination/dir

################################## language pack ##################################
monthsRu=(январь февраль март апрель май июнь июль август сентябрь октябрь ноябрь декабрь)
monthsEn=(january february march april may june july august september october november december)
otherRu='Другое'
otherEn='Other'

monthsSwitch=(${monthsRu[@]})
otherSwitch=$otherRu
###################################################################################

for i in $(ls $dirEntry)
do
    getYearFull=$(exiftool -CreateDate -d %Y $dirEntry/$i)
    getMonthFull=$(exiftool -CreateDate -d %m $dirEntry/$i)
    getYear=${getYearFull:34}
    getMonth=${getMonthFull:34}
    if [[ ${getMonth:0:1} == 0 ]]; then
        getMonth=${getMonth:1}
    fi

    if [[ $getYear ]]; then
        if [[ $getMonth ]]; then
            mkdir -p $dirDest/$getYear/${monthsSwitch[$getMonth-1]^}
            cp -r $dirEntry/$i $dirDest/$getYear/${monthsSwitch[$getMonth-1]^}
        fi
    else
        mkdir -p $dirDest/$otherSwitch
        cp -r $dirEntry/$i $dirDest/$otherSwitch
    fi
done

################################## calc time ##################################
dateEnd=$(date +%H%M%S)
if [[ ${dateStart:0:1} == 0 ]]; then
    dateStart=${dateStart:1}
fi
if [[ ${dateEnd:0:1} == 0 ]]; then
    dateEnd=${dateEnd:1}
fi
resTime=$(($dateEnd-$dateStart))
min=$(($resTime/60))
sec=$(($resTime%60))
echo script running time: $min min, $sec sec
###############################################################################
